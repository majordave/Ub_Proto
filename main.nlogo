extensions [nw]

; Defining breeds for each agent role
breed [roaming-drivers roaming-driver]
breed [occupied-drivers occupied-driver]
breed [waiting-riders waiting-rider]
breed [boarding-riders boarding-rider]
breed [roads road]
directed-link-breed [requests request]
directed-link-breed [signs sign]
undirected-link-breed [joints joint]

; Defining agent-owned variables
roaming-drivers-own [path roam-time t-roam-time profit]
occupied-drivers-own [path ride-time t-roam-time profit]
waiting-riders-own [dpath wait-time fare]

; Global auxiliary variables
globals [
  speed n-rides t-ride-time
  centroid AvgRideTime AvgRoamingTime AvgDriversProfit
  AARideTime AARoamingTime AADriversProfit
  TARideTime TARoamingTime TADriversProfit
]

to set-roads
  ; Creates roads for drivers to move through
  ask patches [
    if (pxcor = -3 and pycor > 3) or
    (pxcor = 3 and pycor < 4) or
    (pycor = -9) or
    (pycor = 3) or
    (pxcor = 9 and pycor > -9) or
    (pxcor = -11 and pycor > 3) or
    (pxcor > 9 and pycor = 10) or
    (pxcor = -7 and pycor < 4) or
    (pycor = 10 and pxcor < -3)
    [sprout-roads 1 [
      set pcolor grey
      ht
    ]]
  ]

  ask roads [
    create-joints-with roads-on neighbors4
  ]
end

to get-centroid
  ; Get current centroid of waiting-riders

  ; Get centroid coordinates
  let x (sum [xcor] of waiting-riders) / NumRiders
  let y (sum [ycor] of waiting-riders) / NumRiders

  ; Set centroid to closest road to coordinates
  set centroid min-one-of roads [distancexy x y]
  ask centroid [set pcolor yellow]
end

to-report get-fare
  ; Calculate fare to certain ride with defined rates

  ; A unit of distance is equal to 2 patches
  let dist length dpath / 2

  ; A unit of time is equal to 10,000 ticks
  let time dist / speed / 100000

  let diff (count waiting-riders) - (count roaming-drivers)
  let surge (diff / 4) + 1
  let this-fare (BaseFare + (DistFare * dist) + (TimeFare * time)) * surge
  report precision this-fare 2
end

to become-occupied
  ; roaming-driver becomes occupied-driver with respective variables

  set t-roam-time (t-roam-time + (ticks - roam-time))

  set breed occupied-drivers
  set ride-time ticks

  ask my-out-links [die]
  set shape "car"
  set color red

  ; The boarding-rider's destination becomes the roaming-driver's destination
  let b-rider one-of waiting-riders-on neighbors
  set path [dpath] of b-rider
  set profit (profit + [fare] of b-rider)
  set profit precision profit 2
  create-sign-to last path [set color orange]
end

to become-roaming
  ; occupied-driver becomes roaming-driver with respective variables
  set n-rides (n-rides + 1)
  set t-ride-time (t-ride-time + (ticks - ride-time))

  set path 0
  set breed roaming-drivers
  set roam-time ticks

  set shape "car"
  set color blue
  ask my-out-links [die]
end

to become-boarding
  ; waiting-rider becomes boarding-rider with respective variables
  ask one-of waiting-riders-on neighbors[
    set breed boarding-riders
    ht
    ask my-links [die]
  ]
end

to advance
  ; Move through path until target is reached

  if (path != 0 and length path > 1)[
    face item 1 path
    fd speed

    if (one-of roads-here != first path)
    [set path but-first path]

    if (not any? roads-here)
    [move-to min-one-of roads [distance myself]]
  ]

end

to go-to-destination
  ; Move occupied-drivers towards boarding-rider's destination

  advance

  if (length path = 1)[
    ask first path [set pcolor grey]
    become-roaming
  ]
end

to move-closer
  ; Move roaming-driver closer to roaming objective

  ; Centroid mode moves towards the centroid of waiting-riders
  if (DriverAI = "Centroid")[
    ; If there's no current path...
    if (path = 0) or
    ; Or centroid has changed...
    ((length path = 1) and ([pcolor] of roads-here != yellow))
    ; Set path to centroid
    [set path [nw:turtles-on-path-to centroid] of one-of roads-here]
  ]

  ; Random mode moves toward a random objective
  if (DriverAI = "Random")[
    ; If there's no current path...
    if (path = 0) or
    ; Or haven't received requests in this location...
    (length path = 1 and count my-links = 0)
    ; Set path to a random road
    [set path [nw:turtles-on-path-to one-of roads] of one-of roads-here]
  ]

  ; Closest mode moves toward the closest waiting-rider
  if (DriverAI = "Closest")[
    ; If there's no current path...
    if (path = 0) or
    ; Or haven't received requests in this location...
    (length path = 1 and count my-links = 0)[
      if (any? waiting-riders)[
        let c-rider min-one-of waiting-riders [distance myself]
        set path [nw:turtles-on-path-to c-rider] of one-of roads-here
      ]
    ]
  ]

  advance

end

to go-to-rider
  ; Move roaming-driver towards the chosen waiting-rider

  if (path != 0 and length path = 2) and
  (any? waiting-riders-on neighbors)[
    become-occupied
    become-boarding
    ask roads with [pcolor = yellow] [set pcolor grey]
  ]
end

to send-request
  ; If waiting-rider haven't sent a request...
  if (not any? my-requests) and
  ; And there's a driver avaible...
  (any? roaming-drivers)[
    ; Send a request to the closest roaming-driver
    create-request-to min-one-of roaming-drivers [distance myself]
    [set color green]
  ]

end

to cancel-request
  ; If waiting time is too long...
  if (not any? in-request-neighbors) and
  (ticks - wait-time > 25000) and
  ; There's a 1/2 proability that the rider cancels the request
  (random 2 = 1)[
    ask my-requests [die]
    set wait-time ticks
  ]
end

to check-requests
  ; Checks available requests and selects the best one
  ask roaming-drivers [
    if (any? in-request-neighbors) and
    (not any? out-request-neighbors)[
      let best-profit in-request-neighbors with-max [fare]
      let target 0

      ifelse (count best-profit = 1) ; If there's only one rider that maximizes profit
      [set target one-of best-profit] ; Select that one
      [set target min-one-of best-profit [distance myself]] ; Else, select the closest one

      set path [nw:turtles-on-path-to target] of one-of roads-here
      create-request-to target [set color blue]
    ]
  ]
end

to gen-riders [n-riders]
  ; Generates a waiting-rider at a valid location, with a valid destination
  ; and default parameters

  create-waiting-riders n-riders [
    set shape "person"
    set color red
    set wait-time ticks

    let rd one-of roads with [
      any? neighbors4 with [pcolor = white] and
      not any? waiting-riders-here
    ]

    move-to [one-of neighbors4 with [pcolor = white]] of rd
    create-joint-with rd

    let target one-of roads with [pcolor = grey and distance myself >= 10]

    ask target [set pcolor orange]

    set dpath nw:turtles-on-path-to target

    create-sign-to target [set color orange]

    set fare get-fare
  ]
end

to New
  ; Main procedure, prepares environment and creates roads and agents
  ca
  reset-ticks
  ask patches [set pcolor white]
  set-roads
  set speed 0.001

  gen-riders NumRiders

  if (DriverAi = "Centroid")
  [get-centroid]

  create-roaming-drivers NumDrivers [
    set shape "car"
    set color blue
    set roam-time ticks
    move-to one-of patches with [pcolor = grey]
  ]
end

to Start
  while [ticks < Duration][
    ; Procedure that updates every tick

    ; If the DriverAI is in centroid mode...
    if (DriverAI = "Centroid") and
    ; And there's no centroid...
    ([pcolor] of centroid != yellow)
    ; Get it
    [get-centroid]

    ask roaming-drivers [
      move-closer
      go-to-rider
    ]

    ; roaming-driver procedure, independent to prevent syncing bug
    check-requests

    ask waiting-riders [
      send-request
      cancel-request
    ]

    ask occupied-drivers [
      go-to-destination
    ]

    if (count waiting-riders < NumRiders) and (random 5000 = 1)
    [gen-riders 1]


    if (n-rides > 0)
    [set AvgRideTime (t-ride-time / n-rides)]

    let drivers turtles with [
      breed = roaming-drivers or
      breed = occupied-drivers
    ]

    set AvgRoamingTime (sum [t-roam-time] of drivers / NumDrivers)
    set AvgDriversProfit (sum [profit] of drivers / NumDrivers)

    set TARoamingTime (TARoamingTime + AvgRoamingTime)
    set TARideTime (TARideTime + AvgRideTime)
    set TADriversProfit (TADriversProfit + AvgDriversProfit)

    if (ticks > 0)[
      set AARoamingTime (TARoamingTime / ticks)
      set AARideTime (TARideTime / ticks)
      set AADriversProfit (TADriversProfit / ticks)
    ]

    tick
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
384
10
785
412
-1
-1
11.91
1
10
1
1
1
0
0
0
1
-16
16
-16
16
0
0
1
ticks
30.0

BUTTON
35
16
191
49
NIL
New
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
215
59
372
92
NumRiders
NumRiders
1
20
20.0
1
1
NIL
HORIZONTAL

SLIDER
214
99
373
132
NumDrivers
NumDrivers
1
20
20.0
1
1
NIL
HORIZONTAL

PLOT
215
141
375
261
DriversAvgProfit
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot AvgDriversProfit\n\n"

PLOT
210
324
370
444
AvgRideTime
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot AvgRideTime\n\n"

BUTTON
35
74
189
107
NIL
Start
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
35
184
192
217
BaseFare
BaseFare
1
10
1.0
1
1
USD
HORIZONTAL

PLOT
35
325
195
445
AvgRoamingTime
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot AvgRoamingTime"

SLIDER
35
227
191
260
DistFare
DistFare
1
10
1.0
1
1
USD
HORIZONTAL

SLIDER
33
272
189
305
TimeFare
TimeFare
0.1
1
0.1
0.1
1
USD
HORIZONTAL

CHOOSER
33
129
192
174
DriverAI
DriverAI
"Centroid" "Random" "Closest"
1

SLIDER
217
15
370
48
Duration
Duration
100000
300000
250000.0
50000
1
ticks
HORIZONTAL

MONITOR
37
455
194
500
Mean AvgRoamingTime
AARoamingTime
3
1
11

MONITOR
208
455
369
500
Mean AvgRideTime
AARideTime
3
1
11

MONITOR
213
270
375
315
Mean AvgDriversProfit
AADriversProfit
3
1
11

@#$#@#$#@
## WHAT IS IT?
A Multi-Agent System for the Management of an Uber-like Transportation System

## HOW IT WORKS

The model has 2 main types of agents:

  1. **drivers** are represented by cars, and have de duty of taking *rider* agents to their destination.
  2. **riders** are represented by persons, and require to arrive at their destinations.

Each agent type alternates between two roles:

  1. **roaming-drivers** are idle drivers able to respond to requests, they move towards the centroid of *waiting-riders* by default, and accept the request from the rider that maximizes profit, breaking ties by choosing the closest one. Represented by blue-colored cars.

  2. **occupied-drivers** are drivers that are currently taking a *boarding-rider* to their desired destination. Represented by red-colored cars.

  3. **waiting-riders** simply send a request to their closest *roaming-driver* and may cancel a request if it takes to long, selecting the new closest one. Represented by red-colored persons.

  4. **boarding-riders** are boarding an *occupied-driver* and simply wait till they arrive at their destination. Invisible, as they're boarding the respective car.

Profit is calculated via the Uber fare formula, which is:

  (*BaseFare* + (*DistFare* *dist*) + (*TimeFare* *time*)) (*surge*)

where:
  
  * *BaseFare:* Price for pickup.
  * *DistFare:* Price per unit of distance.
  * *TimeFare:* Price per unit of time.
  * *dist:* Total units of distance from start to end of trip.
  * *time:* Total units of time from start to end of trip.
  * *surge:* Difference between count of waiting-riders and roaming-drivers.

## HOW TO USE IT

* Use the *NumRiders* slider to set the maximum number of riders.
* Use the *NumDrivers* slider to set the maximum number of drivers.
* Use the *Duration* slider to set the number of ticks the simulation will take.
* Press *New* to create a new environment.
* Press *Start* to begin the simulation.
* Use the *[x]-Fare* sliders to set the values for calculating the profit.
* Use the *DriverAI* chooser to select one of the three possible *roaming-driver* movement modes:
  * **Centroid** is the default method, they move towards the *centroid* of *waiting-riders*.
  * **Random** they move to a random location, till they get a request.
  * **Closest** a greedy approach, they move towards the current closest *waiting-rider*.

 
## THINGS TO NOTICE

  * The *AvgRoamingTime* plot shows the average total time drivers spend roaming, including the time spent to reach the riders.
  * The *AvgRideTime* plot shows the average time each ride/trip takes.
  * The *AvgDriversProfit* plot shows the average total profit accumulated by the drivers so far.
  * Under each plot there is a monitor showing the mean value for each metric for the current time.
  * On the graphical environment on the right, in addition to the main agent types, the following elements appear:
    * Grey patches represent *roads*.
    * The yellow patch represents the current *centroid*.
    * Orange links and patches represent the agents' current *destinations*.
    * Green links represent rider *requests*.
    * Blue links represent driver *acceptance-msg*.

## THINGS TO TRY

* Modifying sliders will create different environments to experiment with. 
* Modifying the duration can let to perform longer or shorter experiments.
* Use the *DriverAI* chooser to perform simulations with the three different movement AI's and compare their behaviors.
* Feel free to modify the *Fare* sliders to represent different types of cities.

## EXTENDING THE MODEL

The *send-request* and *check-requests* procedures are used for defining how *riders* and *drivers* select each other, respectively. Feel free to modify them in order to try a different scenario.
 
## NETLOGO FEATURES

Depends on the NetLogo Nw Extension for pathfinding, and link-breed setting for representing messages.


## CREDITS AND REFERENCES

* Uber fare formula from https://help.uber.com/h/33ed4293-383c-4d73-a610-d171d3aa5a78
* Code available at https://github.com/majordave/Ub_Proto
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
